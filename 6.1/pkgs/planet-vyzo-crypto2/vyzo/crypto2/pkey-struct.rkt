#lang racket/base

(provide (all-defined-out))
(define-struct !pkey (type keygen))
(define-struct pkey (type evp private?))